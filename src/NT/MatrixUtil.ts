module NT {
  /**
   * @constructor
   * @class Matrix
   */
  export class MatrixUtil {


    static lookAt(eye:Float32Array, center:number[], up:number[], dest_matrix:NT.Matrix):void {
      var eye_x = eye[0];
      var eye_y = eye[1];
      var eye_z = eye[2];
      var up_x = up[0];
      var up_y = up[1];
      var up_z = up[2];
      var center_x = center[0];
      var center_y = center[1];
      var center_z = center[2];

      if (eye_x == center_x && eye_y == center_y && eye_z == center_z) {
        return;
        //return dest_matrix.identity();
      }
      var x0:number, x1:number, x2:number; 
      var y0:number, y1:number, y2:number;
      var z0:number, z1:number, z2:number; 
      var l:number;

      z0 = eye_x - center[0];
      z1 = eye_y - center[1];
      z2 = eye_z - center[2];

      l = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);

      z0 *= l;
      z1 *= l;
      z2 *= l;

      x0 = up_y * z2 - up_z * z1;
      x1 = up_z * z0 - up_x * z2;
      x2 = up_x * z1 - up_y * z0;

      l = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);

      if (!l) {
        x0 = 0; x1 = 0; x2 = 0;
      } else {
        l = 1 / l;
        x0 *= l; x1 *= l; x2 *= l;
      }

      y0 = z1 * x2 - z2 * x1;
      y1 = z2 * x0 - z0 * x2;
      y2 = z0 * x1 - z1 * x0;

      l = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
      if (!l) {
        y0 = 0; y1 = 0; y2 = 0;
      } else {
        l = 1 / l;
        y0 *= l; y1 *= l; y2 *= l;
      }

      var dest = dest_matrix.data;
      
      dest[0] = x0; dest[1] = y0; dest[2]  = z0; dest[3]  = 0;
      dest[4] = x1; dest[5] = y1; dest[6]  = z1; dest[7]  = 0;
      dest[8] = x2; dest[9] = y2; dest[10] = z2; dest[11] = 0;
      dest[12] = -(x0 * eye_x + x1 * eye_y + x2 * eye_z);
      dest[13] = -(y0 * eye_x + y1 * eye_y + y2 * eye_z);
      dest[14] = -(z0 * eye_x + z1 * eye_y + z2 * eye_z);
      dest[15] = 1;

      dest_matrix.data = dest;
    }

    static perspective(fovy:number, aspect:number, near:number, far:number, dest_matrix:NT.Matrix) : void{
      var t = near * Math.tan(fovy * Math.PI / 360);
      var r = t * aspect;
      var a = r * 2, b = t * 2, c = far - near;
      var dest = dest_matrix.data;
      dest[0] = near * 2 / a;
      dest[1] = 0;
      dest[2] = 0;
      dest[3] = 0;
      dest[4] = 0;
      dest[5] = near * 2 / b;
      dest[6] = 0;
      dest[7] = 0;
      dest[8] = 0;
      dest[9] = 0;
      dest[10] = -(far + near) / c;
      dest[11] = -1;
      dest[12] = 0;
      dest[13] = 0;
      dest[14] = -(far * near * 2) / c;
      dest[15] = 0;

      dest_matrix.data = dest;
    }
  }
}

