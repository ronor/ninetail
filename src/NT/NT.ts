module NT {
  export var viewport = "viewport";
  export var device_type = undefined;

  export var scale:number = 1;

  /**
   * NT.Sceneの状態種別
   * @property SceneState
   * @type {enum}
   */
  export enum SceneState {
    Begin,
    Main,
    End
  };

  /**
   * ゲームの状態種別
   * @property GameState
   * @type {enum}
   */
  export enum GameState {
    Play,
    Pause
  };
}
