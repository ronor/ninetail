module NT {
  export class Scene {
    /**
     * 内包するレイヤー
     * @property layers
     * @type {[key:number]:NT.Graphics.Layer}
     * @default undefined
     * @protected
     */
    private layers:{[key:number]:NT.Graphics.Layer;}

    /**
     * シーン開始時の処理で一回だけ実行されます。
     * @property begin_process
     * @type {Function}
     * @default undefined
     * @private
     */
    private begin_process:Function;

    /**
     * シーンのメイン処理です。
     * @property main_process
     * @type {Function}
     * @default undefined
     * @private
     */
    private main_process:Function;
    //終了処理

    /**
     * シーンの終了時(シーン変更時)に呼ばれる終了処理です。
     * @property end_process
     * @type {Function}
     * @default undefined
     * @protected
     */
    public end_process:Function;
    mouse_event:{[key:string]:()=>void;} = {};

    state:number = SceneState.Begin;

    /**
     * ゲームシーン(タイトル、ゲーム本編、ゲームオーバーなど)を表すクラスです。
     *
     * @constructor
     * @class Scene
     * @param {string} scene_name シーンの名称。呼び出すシーンを切り替える際は、この名前を指定します。シーン名は一意な値である必要があります。
     */

    constructor(public scene_name:string) {
      this.layers = {};
      
    }

    /**
     * シーンの基本初期化処理。Scene.executeが呼び出され、stateがSceneStage.Beginの場合に実行されます。
     * @method init
     */
    private init():void {
      //イベント初期化
      var event_type = ["mousedown", "mouseup", "mousemove"];
      var event_type_touch = ["touchstart", "touchend", "touchmove"];
      for (var i:number = 0; i< event_type.length; i++) {
        if (this.mouse_event[event_type[i]] != undefined) {
          if (window.addEventListener){
            if (device_type == "mobile") {
              document.addEventListener(event_type_touch[i], this.mouse_event[event_type[i]] , true);
            } else {
              document.addEventListener(event_type[i], this.mouse_event[event_type[i]] , true);
            }
          } 
        }
      }

      //レイヤー初期化
      for (var key in this.layers) {
        console.log("init layer:" + key);
        this.layers[key].createCanvas();
        this.layers[key].initContext();
      }
    }

    /**
     * シーン開始時の処理登録
     * @method registerBeginProcess
     * @param {Function} f 実行する処理(関数)
     */
    registerBeginProcess(f:Function):void {
      if(this.begin_process == undefined) this.begin_process = f;
    }

    /**
     * シーン処理登録
     * @method registerMainProcess
     * @param {Function} f 実行する処理(関数)
     */
    registerMainProcess(f:Function):void {
      if(this.main_process == undefined) this.main_process = f;
    }

    /**
     * シーン終了処理登録
     * @method registerEndProcess
     * @param {Function} f 実行する処理(関数)
     */
    registerEndProcess(f:Function):void {
      if(this.end_process == undefined) this.end_process = f;
    }

    /**
      * マウス(タッチ)入力系イベント処理を登録。
      * mousedownはtouchstart, mouseupはtouchend、mosuemoveはtouchmoveに対応します。
      * @param {string} type イベントタイプ(mouseup, mousedown, mouseup)
      */
    addEvent(type:string, f:()=>void) {
      if (this.mouse_event[type] == undefined) this.mouse_event[type] = f;
    }

    /**
     * シーン名を取得
     * @return {string} シーン名
     */
    getName():string {
      return this.scene_name;
    }

    /**
     * レイヤーを追加
     * @method addLayer
     * @param {Layer} layer 追加するレイヤーオブジェクト
     */
    addLayer(layer:NT.Graphics.Layer, tag:number) {
      this.layers[tag] = layer;
      console.log(this.layers[tag]);
    }

    /**
     * レイヤーを取得
     */
    getLayer(tag:number):NT.Graphics.Layer {
      return this.layers[tag];
    }

    /**
     * シーン処理実行
     * @method execute
     */
    execute():void {
      if (this.state == SceneState.Begin) {
        //シーン初期化
        this.init();
        if (this.begin_process != undefined) this.begin_process.apply(null);
        this.state = SceneState.Main;

      } else if (this.state == SceneState.Main) {
        //シーンメイン処理
        if (this.main_process != undefined) this.main_process.apply(null);
        this.draw();

      } else if (this.state == SceneState.End) {
        //シーン終了処理
        if (this.end_process != undefined) this.end_process.apply(null);
        this.destroy();
      }
    }

    /**
     * シーン終了時、シーンの破棄を行うメソッド
     * @method destroy
     */
    private destroy():void {
      for (var key in this.layers) {
        this.layers[key].destroy();
      }

      //イベントの削除
      var event_type = ["mousedown", "mouseup", "mousemove"];
      var event_type_touch = ["touchstart", "touchend", "touchmove"];
      for (var i:number = 0; i< event_type.length; i++) {
        if (this.mouse_event[event_type[i]] != undefined) {
          if (window.removeEventListener){
            if (device_type == "mobile") {
              document.removeEventListener(event_type_touch[i], this.mouse_event[event_type[i]], true);
            } else {
              document.removeEventListener(event_type[i], this.mouse_event[event_type[i]] , true);
            }
          } 
        }
      }
    }


    /**
     * シーン描画処理
     * @method draw
     */
    private draw():void {
      for (var key in this.layers) {
        if (this.layers[key].getVisible()) this.layers[key].draw(null);
      }
    }
  }
}
