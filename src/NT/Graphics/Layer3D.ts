interface WebGLProgram extends WebGLObject {
  vertexPositionAttribute:number;
  vertexColorAttribute:number;
  pMatrixUniform:WebGLUniformLocation;
  mvMatrixUniform:WebGLUniformLocation;
}
module NT.Graphics.GL {
  export class StructModelBuffer {
    vbo:WebGLBuffer;
    cvbo:WebGLBuffer;
    ibo:WebGLBuffer;
    tvbo:WebGLBuffer;
    attr_location:NT.Graphics.GL.StructLocation;
    attr_size:NT.Graphics.GL.StructAttrSize;
    index_size:number;
    texture:WebGLTexture;

    constructor(vbo:WebGLBuffer, cvbo:WebGLBuffer, ibo:WebGLBuffer, tvbo?:WebGLBuffer) {
      this.vbo = vbo;
      this.cvbo = cvbo;
      this.ibo = ibo;

      if (tvbo) this.tvbo = tvbo;
    }

    createTexture(gl:WebGLRenderingContext, filename:string) {
      var texture_image = new Image();
      var tex:WebGLTexture;
      texture_image.onload = () => {
        // テクスチャオブジェクトの生成
        tex = gl.createTexture();

        // テクスチャをバインドする
        gl.bindTexture(gl.TEXTURE_2D, tex);
        
        // テクスチャへイメージを適用
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_image);
        
        // ミップマップを生成
        gl.generateMipmap(gl.TEXTURE_2D);
        
        // テクスチャのバインドを無効化
        gl.bindTexture(gl.TEXTURE_2D, null);

        this.texture = tex;
      };
      texture_image.src = filename;
    }

    setTexture(tex:WebGLTexture):void {
      this.texture = tex;
    }

    setIndexSize(index_size:number) {
      this.index_size = index_size;
    }

    setLocation(attr_location:NT.Graphics.GL.StructLocation):void {
      this.attr_location = attr_location;
    }
    setAttrSize(attr_size:NT.Graphics.GL.StructAttrSize):void {
      this.attr_size = attr_size;
    }
  }

  export class StructAttrSize {
    vertex:number;
    color:number;
    texture:number;
    constructor(vertex:number, color:number, texture?:number) {
      this.vertex = vertex;
      this.color = color;
      if (texture) this.texture = texture;

    }
  }

  export class StructLocation {
    vertex:number;
    color:number;
    texture:number;
    constructor(vertex:number, color:number, texture?:number) {
      this.vertex = vertex;
      this.color = color;
      if (texture) this.texture = texture;
    }
  }

  export class StructModel {
    vertex:number[];
    color:number[];
    index:number[];
    texture_coord:number[];
    constructor(vertex:number[], color:number[], index:number[], texture_coord?:number[]) {
      this.vertex = vertex;
      this.color = color;
      this.index = index;
      if (texture_coord) {
        this.texture_coord = texture_coord;
      }
    }
  }
}

module NT.Graphics {
  /**
   * WebGLレンダリング可能なLayer
   * @constructor
   * @class Layer3D
   * @extends Layer
   * @param {number} width レイヤーの幅
   * @param {number} height レイヤーの高さ
   * @param {number} background_color レイヤーの背景色。省略した場合は透過
   */
  export class Layer3D extends NT.Graphics.Layer {
    _gl:WebGLRenderingContext;
    _program:WebGLProgram;
    tmp_texture:WebGLTexture;
    constructor(width:number, height:number, background_color:number) {
      super(width, height, background_color);
    }

    initContext():void {
      this._gl = this.canvas.getContext("webgl") || this.canvas.getContext("experimental-webgl");
    }

    get gl():WebGLRenderingContext {
      return this._gl;
    }

    get program():WebGLProgram {
      return this._program;
    }

    set program(program:WebGLProgram) {
      this._program = program;
    }

    /**
     * 表示優先度を設定
     * @override
     */
    setPriority(priority_index:number):void {
      super.setPriority(priority_index);
      this.canvas.style.zIndex = priority_index + '';
    }


    /**
     * シェーダーを記述しているscript要素のidを渡し、シェーダーの初期化を行います。
     * @param {string} vertex_shader_id
     * @param {string} fragment_shader_id
     * @return {boolean} 失敗した場合はfalse
     */
    initShaders(vertex_shader_id:string, fragment_shader_id:string):boolean {
      this.program = this.loadProgram(this.getShaderFromId(vertex_shader_id), this.getShaderFromId(fragment_shader_id));
      console.log(this.program);
      if (!this.program) return false;
      this.gl.useProgram(this.program);
      return true;
    }

    /**
     * 頂点属性の変数名をセット
     * @method setVertexPositionAttribute
     * @param {string} position_name VertexShader内で定義した頂点座標用の変数名
     * 
     */
    setVertexPositionAttribute(position_name:string):void {
      this.program.vertexPositionAttribute = this.gl.getAttribLocation(this.program, position_name);
      this.gl.enableVertexAttribArray(this.program.vertexPositionAttribute);
    }

    /**
     * @method setVertexColorAttribute
     * @param {string} vcolor_name
     */
    setVertexColorAttribute(vcolor_name:string):void {
      this.program.vertexColorAttribute = this.gl.getAttribLocation(this.program, vcolor_name);
      this.gl.enableVertexAttribArray(this.program.vertexColorAttribute);
    }

    /**
     * @method setMatrixUniform 
     * @param {string} p_matrix_name
     * @param {string} mv_matrix_name
     */
    setMatrixUniform(p_matrix_name:string, mv_matrix_name:string):void {
      this.program.pMatrixUniform = this.gl.getUniformLocation(this.program, p_matrix_name);
      this.program.mvMatrixUniform = this.gl.getUniformLocation(this.program, mv_matrix_name);
    }


    /**
     * 指定したidの要素(script要素)からシェーダーを取得し、WebGLShaderを作成して返します。
     * script要素のtypeが"x-shader/x-fragment"の場合、フラグメントシェーダーとして処理を行います。
     * typeが"x-shader/x-vertex"の場合、頂点シェーダーとして処理を行います。
     * @param {string} id 
     * @return WebGLShader
     */
    getShaderFromId(id:string):WebGLShader {
      var shader:WebGLShader = null;
      var shader_script = document.getElementById(id);
      if (!shader_script) {
        console.log("error:not found shader id");
        return null;
      }

      var str = "";
      var k = shader_script.firstChild;
      while (k) {
        if (k.nodeType == 3) {
          str += k.textContent;
        }
        k = k.nextSibling;
      }

      if (shader_script.getAttribute("type") == "x-shader/x-fragment") {
        shader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
      } else if (shader_script.getAttribute("type") == "x-shader/x-vertex") {
        shader = this.gl.createShader(this.gl.VERTEX_SHADER);
      } else {
        console.log("error: type is not valid");
        return null;
      }

      this.gl.shaderSource(shader, str);
      this.gl.compileShader(shader);

      if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
        console.log("SHADER COMPILE STATUS");
        console.log(this.gl.getShaderInfoLog(shader));
        return null;
      } else {
        console.log(id + "COMPILE SUCCESS");
      }
      return shader;
    }

    /**
     * シェーダーオブジェクト（WebGLShader)からWebGLProgramを作成し返します
     * @param {WebGLShader} vertex_shader
     * @param {WebGLShader} fragment_shader
     * @return WebGLProgram
     */
    loadProgram(vertex_shader:WebGLShader, fragment_shader:WebGLShader):WebGLProgram {
      if (!vertex_shader || !fragment_shader)  {
        console.log("shader not found");
        return null;
      }

      //WebGLProgramオブジェクト作成
      var program = this.gl.createProgram();

      //頂点シェーダーをアタッチ
      this.gl.attachShader(program, vertex_shader);
      //フラグメントシェーダーをアタッチ
      this.gl.attachShader(program, fragment_shader);
      
      //シェーダーをシェーダープログラムへリンク
      this.gl.linkProgram(program);

      //リンクの状態を取得し、エラーの場合はエラー内容を出力
      var linked = this.gl.getProgramParameter(program, this.gl.LINK_STATUS);
      if (!linked) {
        var last_error = this.gl.getProgramInfoLog(program);
        console.log("Error in program linking:" + last_error);
        this.gl.deleteProgram(program);
        return null
      } else {
        console.log("program linking success");
      }
      return program;
    }

    /**
     * 頂点バッファ作成
     */
    createVBO(data:number[]):WebGLBuffer {
      var vbo = this.gl.createBuffer();
      //vboオブジェクトを配列バッファとしてバインド
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, vbo);
      //バッファデータをvboオブジェクトにセット
      this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(data), this.gl.STATIC_DRAW);
      //バインドできるバッファは同時に１つだけのため、nullをセットしてバインドを解除
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
      return vbo;
    }

    /**
     * インデックスバッファ作成
     */
    createIBO(data:number[]):WebGLBuffer {
      var ibo = this.gl.createBuffer();
      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, ibo);
      this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Int16Array(data), this.gl.STATIC_DRAW);
      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, null);
      return ibo;
    }


    prepareModel(data:NT.Graphics.GL.StructModel, attr_size:NT.Graphics.GL.StructAttrSize, attr_location:NT.Graphics.GL.StructLocation):NT.Graphics.GL.StructModelBuffer {
      var gl = this.gl;
      var vbo = this.createVBO(data.vertex);
      gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
      gl.enableVertexAttribArray(attr_location.vertex);
      gl.vertexAttribPointer(attr_location.vertex, attr_size.vertex, gl.FLOAT, false, 0, 0);

      var cvbo = this.createVBO(data.color);
      gl.bindBuffer(gl.ARRAY_BUFFER, cvbo);
      gl.enableVertexAttribArray(attr_location.color);
      gl.vertexAttribPointer(attr_location.color, attr_size.color, gl.FLOAT, false, 0, 0);

      var ibo = this.createIBO(data.index);
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);


      var tvbo = null;
      if (data.texture_coord) {
        tvbo = this.createVBO(data.texture_coord);
        gl.bindBuffer(gl.ARRAY_BUFFER, tvbo);
        gl.enableVertexAttribArray(attr_location.texture);
        gl.vertexAttribPointer(attr_location.texture, attr_size.texture, gl.FLOAT, false, 0, 0);
      }

      var buffer = new NT.Graphics.GL.StructModelBuffer(vbo, cvbo, ibo, tvbo);
      buffer.setIndexSize(data.index.length);
      buffer.setAttrSize(attr_size);
      buffer.setLocation(attr_location);
      return buffer;
    }

    /**
     * NT.Graphics.GL.StructModelBufferを使用し物体を描画
     * @param {NT.Graphics.GL.StructModelBuffer} buffer
     */
    drawElement(buffer:NT.Graphics.GL.StructModelBuffer, num:number) {
      var gl = this.gl;
      gl.bindBuffer(gl.ARRAY_BUFFER, buffer.vbo);
      gl.vertexAttribPointer(buffer.attr_location.vertex, buffer.attr_size.vertex, gl.FLOAT, false, 0, 0);
      gl.bindBuffer(gl.ARRAY_BUFFER, buffer.cvbo);
      gl.vertexAttribPointer(buffer.attr_location.color, buffer.attr_size.color, gl.FLOAT, false, 0, 0);
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer.ibo);

      if (buffer.texture) {
        gl.bindTexture(gl.TEXTURE_2D, buffer.texture);
      }

      gl.drawElements(gl.TRIANGLES, buffer.index_size, gl.UNSIGNED_SHORT, 0);

      if (buffer.texture) {
        gl.bindTexture(gl.TEXTURE_2D, null);
      }

    }

    
  }
  
}
