module NT.Graphics {

  export class Layer extends DrawableElement{
    canvas:HTMLCanvasElement;
    ctx:CanvasRenderingContext2D;
    image_data:ImageData;

    /**
     * NT.LayerはCanvasを内包する描画領域で、NT.Sceneに追加して使用します。
     * NT.Sceneは1つ以上のNT.Layerを持つことができ、NT.Layerは重ねて複数用いることができます。
     * 再描画する必要のない描画物（背景等）は別のNT.Layerとして表示することで、
     * 描画処理の負担を軽減します。
     *
     * @constructor
     * @class Layer
     * @extends DrawableElement
     * @param {number} width レイヤーの幅
     * @param {number} height レイヤーの高さ
     * @param {number} background_color レイヤーの背景色。省略した場合は透過
     */
    constructor(width:number, height:number, background_color:number) {
      super(width, height);
      this.setBackgroundColor(background_color);
    }

    /**
     * Canvasを生成し初期化します
     * CanvasのサイズがNT.Layer生成時に指定されていない場合は、NT.Coreに与えられたサイズで初期化されます。
     * @method createCanvas
     */
    public createCanvas():void {
      var container = document.getElementById(viewport);
      if (this.width == 0 && this.height == 0) {
        this.width = parseInt(container.style.width);
        this.height = parseInt(container.style.height);
      } 

      this.canvas = document.createElement("canvas");
      this.canvas.style.width = (this.width * scale) + "px";
      this.canvas.style.height = (this.height * scale) + "px";
      this.canvas.style.position = "absolute";
      this.canvas.style.top = "0";
      this.canvas.style.left = "0";

      if (this.background_color) {
        console.log("BG COLOR = " + this.background_color);
        this.canvas.style.backgroundColor = NT.Graphics.Color.toCss(this.background_color);
      }
      this.canvas.setAttribute('width', this.width.toFixed());
      this.canvas.setAttribute('height', this.height.toFixed());
      container.appendChild(this.canvas);
    }

    initContext():void {
      this.ctx = this.canvas.getContext("2d");
    }



    /**
     * Canvasの描画内容をクリアします
     * @method clearCanvas
     */
    clearCanvas():void {
      this.ctx.clearRect(0, 0, this.width, this.height);
    }
    

    /**
     * 描画処理
     * @method draw
     */
    draw(ctx:CanvasRenderingContext2D):void {
      if (this.ctx) {
        this.clearCanvas();
        for(var key in this.drawable_elements) {
          if (this.drawable_elements[key].getVisible()) {
            this.drawable_elements[key].draw(this.ctx);
          }
        }
      }
    }

    /**
     * Canvasに追加されたエレメントがある場合にはそれを削除した上で、Canvasを削除する
     */
    destroy():void {
      var container = document.getElementById(viewport);
      for (var i:number = 0; i< this.canvas.childNodes.length; i++) {
        this.canvas.removeChild(this.canvas.childNodes[i]);
      }
      container.removeChild(this.canvas);
    }

    /**
     * 表示優先度を設定
     * @override
     */
    setPriority(priority_index:number):void {
      super.setPriority(priority_index);
      this.canvas.style.zIndex = priority_index + '';
    }
  }
}
