module NT.Graphics {
  export class Color {

    static toCss(color:number):string {
      var r = color >> 24 & 0xFF;
      var g = color >> 16 & 0xFF;
      var b = color >> 8 & 0xFF;
      var a = color & 0xFF;
      return "rgb(" + r + "," + g + "," + b + ")";
    }

    constructor(public r:number, public g:number, public b:number, public a:number) {
      if (!a) {
        this.a = 0xFF;
      }
    }

    getR():number {
      return this.r;
    } 

    getG():number {
      return this.g;
    }

    getB():number {
      return this.b;
    }

    getAlpha():number {
      return this.a;
    }

    getFloatR():number {
      return this.r / 255;
    }

    getFloatG():number {
      return this.g / 255;
    }

    getFloatB():number {
      return this.b / 255;
    }

    getFloatAlpha():number {
      return this.a / 255;
    }

    getCss():string {
      return "rgb(" + this.r + "," + this.g + "," + this.b + ")";
    }
  }
}
