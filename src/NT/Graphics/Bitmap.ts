module NT.Graphics {
  /**
   * Bitmapクラス
   */
  export class Bitmap {
    private image:HTMLImageElement;
    private is_loaded:boolean = false;

    constructor(file:string) {
      this.image = new Image();
      this.image.src = file + "?" + new Date().getTime();
      this.image.onload = () => {
        this.is_loaded = true;
      }
    }

    getImage():HTMLImageElement {
      return this.image;
    }

    isLoaded():boolean {
      return this.is_loaded;
    }
  }
}
