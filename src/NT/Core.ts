module NT {
  export class Core {
    state:number = GameState.Play;
    current_scene_name:string; 
    scenes:{ [key:string]:Scene; } = {};

    /**
     * シーン管理、ゲーム進行の管理(一時停止等）を管理するクラスです。
     * 通常１つのCoreクラスをインスタンス化し、そこに1つ以上のSceneクラスのインスタンスを追加します。
     * @class Core
     * @constructor 
     * @param {number} width
     * @param {number} height
     * @param {number} color
     */
    constructor(public width:number, public height:number, background_color:number) {
      this.current_scene_name = null;
      this.state = GameState.Play;

      var user_agent = window.navigator.userAgent.toLowerCase();
      if (user_agent.indexOf("mobile") != -1) {
        device_type = "mobile";
      } else {
        device_type = "other";
      }

      //表示領域のサイズを取得
      var screen_width = document.documentElement.clientWidth;
      var screen_height = document.documentElement.clientHeight
      var body = document.getElementsByTagName("body");
      body[0].style.width = screen_width + "px";
      body[0].style.height = screen_height + "px";
      if (background_color) {
        body[0].style.backgroundColor = NT.Graphics.Color.toCss(background_color);
      }
      body[0].style.display = "table-cell";
      body[0].style.verticalAlign = "middle";
      body[0].style.margin = "0";

      //Canvasのコンテナ用divを作成
      var container:HTMLElement = document.createElement("div");
      container.id = viewport;
      container.style.overflow = "hidden";
      container.style.width = (width * scale) + "px";
      container.style.height = (height * scale) + "px";
      container.style.position = "relative";
      container.style.margin = "0 auto";
      document.body.appendChild(container);

    }
      
    /**
     * NT.Sceneを追加する
     * @method addScene
     * @param {NT.Scene} sceneオブジェクト
     */
    addScene(scene:Scene) {
      this.scenes[scene.getName()] = scene;
    }

    /**
     * 指定したNT.Sceneに切り替える
     * @method changeScene
     * @param {string} scene_name シーン識別用の名前
     */
    changeScene(scene_name:string):void {
      if (this.current_scene_name != null) {
        //現在実行中のシーンがある場合は、そのシーンのステートを終了状態にして終了処理を実行する。
        this.scenes[this.current_scene_name].state = SceneState.End;
        this.scenes[this.current_scene_name].execute();
      }
      //変更先シーンのステートを開始状態にする
      this.current_scene_name = scene_name;
      this.scenes[this.current_scene_name].state = SceneState.Begin;
    }

    /**
     * シーンを実行します。
     * シーンに定義した処理を継続的に実行する場合、このexecuteメソッドを適切な間隔で呼び出す必要があります。
     * @method execute
     */
    execute():void {
      if (this.state == GameState.Play) {
        //ゲーム処理実行中の場合はシーンの実行を行う
        if (this.current_scene_name != null) {
          this.scenes[this.current_scene_name].execute();
        } else {
          console.log("please set scene");
        }
      }
    }

    /**
     * ゲーム処理を開始します
     * @method play
     */
    play():void {
      this.state = GameState.Play;
    }

    /**
     * ゲーム処理を一時停止状態にします
     * @method pause
     */
    pause():void {
      this.state = GameState.Pause;
    }
  }
}
