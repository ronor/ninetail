var NT;
(function (NT) {
    var Rect = (function () {
        /**
        * 矩形
        * @class Rect
        * @constructor
        */
        function Rect(x, y, width, height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
        Rect.prototype.containsPoint = function (point) {
            if (point.getX() >= this.x && point.getX() <= this.x + this.width && point.getY() >= this.y && point.getY() <= this.y + this.height) {
                return true;
            } else {
                return false;
            }
        };
        return Rect;
    })();
    NT.Rect = Rect;
})(NT || (NT = {}));
