
var width = 480;
var height = 270;

NT.scale = 1.0;
var nt = new NT.Core(width, height, 0x00 << 24 | 0x00 << 16 | 0x00 << 8 | 0xFF);


var scene = new NT.Scene("first");
var scene2 = new NT.Scene("second");
nt.addScene(scene);
nt.addScene(scene2);

var L_NORMAL1 = 100;
var L_NORMAL2 = 101;
var L_3D = 102;
var L_BITMAP = 103;


var layers = [
  new NT.Graphics.Layer(width, height),
  new NT.Graphics.Layer(),
  new NT.Graphics.Layer3D(width, height),
  new NT.Graphics.LayerBitmap(),
];
scene.addLayer(layers[0], L_NORMAL1);
scene.addLayer(layers[1], L_NORMAL2);
scene.addLayer(layers[2], L_3D);
scene.addLayer(layers[3], L_BITMAP);

scene.getLayer(L_NORMAL1).setVisible(true);
scene.getLayer(L_BITMAP).setVisible(false);
scene.getLayer(L_NORMAL2).setVisible(false);



var sprite = new NT.Graphics.Sprite();
sprite.setPosition(10,10);
sprite.setSize(300,100);
sprite.setText(document.documentElement.clientWidth + ":" + document.documentElement.clientHeight, new NT.Graphics.Font("Arial","20","px"), new NT.Graphics.Color(0,255,0));
sprite.setBackgroundColor(0xFF << 24 | 0x00 << 16 | 0x00 << 8 | 0xFF);
scene.getLayer(L_NORMAL1).addElement(sprite, 100);


var sp = new NT.Graphics.Sprite(new NT.Graphics.Bitmap("./resources/image.png"), new NT.Rect(0,0,16,16));
sp.setPosition(0,50);
sp.setSize(16,16);
sp.setBackgroundColor(0xFF << 24 | 0x00 << 16 | 0x00 << 8 | 0xFF);
scene.getLayer(L_NORMAL1).addElement(sp, 101);


scene.getLayer(L_NORMAL1).addElement(new NT.Graphics.Sprite(), 102);
var sprite2 = scene.getLayer(L_NORMAL1).getElement(102);
sprite2.setPosition(100,200);
sprite2.setSize(50,50);
sprite2.setBackgroundColor(0x00 << 24 | 0x00 << 16 | 0xFF << 8 | 0xFF);

var process = function(time) {
  nt.execute();
  window.requestAnimationFrame(process);
};


var ball_bitmap = new NT.Graphics.Bitmap("./resources/image.png");
var balls = new Array(50);
for(var i=0; i<balls.length; i++) {
  balls[i]  = new NT.Graphics.Sprite(ball_bitmap, new NT.Rect(3*16,0,16,16));
  balls[i].setPosition(100,100);
  balls[i].setSize(16,16);
  scene.getLayer(L_NORMAL1).addElement(balls[i],103 + i);
}




var y = 0;
var ball_x = new Array(100);
var ball_y = new Array(100);
var move_x = new Array(100);
var move_y = new Array(100);
var vbo1, vcbo1, ibo1;
var vbo2, vcbo2, ibo2;

var buffer1;
var buffer2;
scene.registerBeginProcess(function() {
  console.log("scene1 begin");
  y = 0;
  for(var i=0; i<balls.length; i++) {
    ball_x[i] = Math.floor(Math.random() * 480);
    ball_y[i] = Math.floor(Math.random() * 320);
    move_x[i] = Math.floor(Math.random() * 3) + 1;
    move_y[i] = Math.floor(Math.random() * 3) + 1;
    balls[i].setPositionX(ball_x[i]);
    balls[i].setPositionY(ball_y[i]);
  }

  scene.getLayer(L_NORMAL1).setPriority(6);
  scene.getLayer(L_3D).setPriority(5);

  var l3d = scene.getLayer(L_3D);
  l3d.setVisible(true);

  if(!l3d.initShaders("vertex_shader", "fragment_shader")){
    console.log("ERROR: initShaders");
  }

  //WebGL Layer3D
  var gl = l3d.gl;

  gl.activeTexture(gl.TEXTURE0);

  //attributeLocationを配列に取得
  var attLocation = [
    gl.getAttribLocation(l3d.program, 'aVertexPosition'),
    gl.getAttribLocation(l3d.program, 'aVertexColor'),
    gl.getAttribLocation(l3d.program, 'aVertexTexture'),
  ];

  buffer1 = createModel1(l3d, attLocation);
  buffer2 = createModel2(l3d, attLocation);
});


var rad = 0;

scene.registerMainProcess(function() {

//  console.dir(l3d.tmp_texture);
  //console.dir(buffer2.texture);
  var mat = NT.MatrixUtil;
  var l3d = scene.getLayer(L_3D);
  var gl = l3d.gl;
  
  gl.clearColor(0.5, 0.0, 0.0, 0.0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  gl.enable(gl.DEPTH_TEST);

  var vMatrix = new NT.Matrix().identity();
  var mMatrix = new NT.Matrix().identity();
  var pMatrix = new NT.Matrix().identity();

  mat.lookAt([0.0, 0.0, 3.0], [0, 0, 0], [0, 1, 0], vMatrix);
  mat.perspective(45, width / height, 0.1, 100, pMatrix);
  var mvpMatrix = null;

  var uniLocations = [
    gl.getUniformLocation(l3d.program, 'mvpMatrix'),
    gl.getUniformLocation(l3d.program, 'texture')
  ];


  var stack = new NT.Stack();

  //現在のモデル行列をスタックに積む
  stack.push(mMatrix.copy());

  if (rad >= 360) rad = 0;
  mMatrix = mMatrix.translate(new NT.Vector3(0.0, -1.0, 0.0));
  mMatrix = mMatrix.rotate(rad,new NT.Vector3(0, 0, 1));
  //mMatrix = mMatrix.scale(new NT.Vector3(1.0,0.5,1.0));
  rad += 0.01;

  gl.bindTexture(gl.TEXTURE_2D, buffer1.texture);
  gl.uniform1i(uniLocations[1], 0);
  mvpMatrix = pMatrix.x(vMatrix).x(mMatrix);
  //三角形uniform転送
  gl.uniformMatrix4fv(uniLocations[0], false, mvpMatrix.data);
  l3d.drawElement(buffer1, 2);


  //平行移動行列をかけたモデル行列を破棄しスタックに積んでいたもともとのモデル行列を取り出す
  mMatrix = stack.pop();

  gl.bindTexture(gl.TEXTURE_2D, buffer2.texture);
  gl.uniform1i(uniLocations[1], 0);
  mMatrix = mMatrix.translate(new NT.Vector3(1.0, 0.0, 0.0));
  mvpMatrix = pMatrix.x(vMatrix).x(mMatrix);
  gl.uniformMatrix4fv(uniLocations[0], false, mvpMatrix.data);
  l3d.drawElement(buffer2, 2);



  gl.flush();


  /*
  mat.multiply(vMatrix, mMatrix, mvMatrix);
  var uniPLocation = gl.getUniformLocation(l3d.program, 'uPMatrix');
  var uniMVLocation = gl.getUniformLocation(l3d.program, 'uMVMatrix');
  gl.uniformMatrix4fv(uniPLocation, false, pMatrix);
  gl.uniformMatrix4fv(uniMVLocation, false, mvMatrix);
  */
  


  

  for(var i=0; i<balls.length; i++) {
    ball_x[i] += move_x[i];
    ball_y[i] += move_y[i];

    if (ball_x[i] > 480 - balls[i].width) {
      move_x[i] = -1 * (Math.floor(Math.random() * 3) + 1);
    } else if (ball_x[i] < 0) {
      move_x[i] =  Math.floor(Math.random() * 3) + 1;
    }

    if (ball_y[i] > 270 - balls[i].height) { 
      move_y[i] = -1 * (Math.floor(Math.random() * 3) + 1);
    } else if (ball_y[i] < 0) {
      move_y[i] = Math.floor(Math.random() * 3) + 1;
    }

    balls[i].setPositionX(ball_x[i]);
    balls[i].setPositionY(ball_y[i]);

  }

  /*
  var color = 0x00 << 24 | 0xFF << 16 | 0xFF << 8 | 0xFF;
  layers[1].setForegroundColor(color);
  layers[1].drawLine(1,1,200,200);
  */

});

scene.registerEndProcess(function() {
  console.log("scene1 end");
});

scene.addEvent("mousedown", function(e){
  var rect = e.target.getBoundingClientRect();
  var mx = e.clientX - rect.left;
  var my = e.clientY - rect.top;


  /*
  if(sprite.getRect().containsPoint(new NT.Point(mx, my))) {
    nt.changeScene("second");
  }*/

  //y += 1;
  //console.log(y);
  //nt.changeScene("second");
  /*
  if (layers[0].getPriority() == 0){
    layers[0].setPriority(1);
    layers[1].setPriority(0);
  } else {
    layers[0].setPriority(0);
    layers[1].setPriority(1);
  }*/
});

//scene.addEvent("mousemove", function() {
//});

scene2.registerBeginProcess(function() {
  console.log("scene2 begin");
});

scene2.registerMainProcess(function() {
  console.log("scene2 main");
});

scene2.registerEndProcess(function() {
  console.log("scene2 end");
});


scene2.addEvent("mousedown", function() {
  nt.changeScene("first");
});

//scene2.addEvent("mousemove", function() {
//});

   var input = document.createElement("input");
   input.type = "text";
   input.id = "test_input";
   input.style.borderWidth="1px";
   input.style.borderStyle="solid";
   input.style.borderColor="#FFF";
   input.style.borderRadius="0";
   input.style.backgroundColor="#000";
   input.style.color = "#FFF";
   document.body.appendChild(input);

nt.changeScene("first");

window.requestAnimationFrame(process);


function createModel1(l3d, attLocation) {
  
  var vertices = [
    -0.5,  0.5, 0.0,
    0.5,  0.5, 0.0,
    -0.5, -0.5, 0.0,
    0.5, -0.5, 0.0
  ];

  var colors = [
    1.0, 1.0, 1.0, 0.0,
    1.0, 1.0, 1.0, 0.0,
    1.0, 1.0, 1.0, 0.0,
    1.0, 1.0, 1.0, 0.0,
  ];
  var texture_coord = [
    0.0, 0.0,
    1.0, 0.0,
    0.0, 1.0,
    1.0, 1.0,
  ];

  var indeces = [
    0,1,2,
    3,2,1
  ];
  var NTGL = NT.Graphics.GL;
  var buffer = l3d.prepareModel(new NTGL.StructModel(vertices, colors, indeces, texture_coord), 
      new NTGL.StructAttrSize(3,4,2),
      new NTGL.StructLocation(attLocation[0], attLocation[1], attLocation[2]));
  buffer.createTexture(l3d.gl, "/dungeons_holic/public_html/images/miku2.gif");

  return buffer;
}


function createModel2(l3d, attLocation) {

  var vertices = [
    -0.5,  0.5, 0.0,
    0.5,  0.5, 0.0,
    -0.5, -0.5, 0.0,
    0.5, -0.5, 0.0
  ];
  
  var colors = [
    1.0, 1.0, 1.0, 0.0,
    1.0, 1.0, 1.0, 0.0,
    1.0, 1.0, 1.0, 0.0,
    1.0, 1.0, 1.0, 0.0,
  ];

  var texture_coord = [
    0.0, 0.0,
    1.0, 0.0,
    0.0, 1.0,
    1.0, 1.0,
  ];

  var indeces = [
    0,1,2,
    3,2,1
  ];

  var NTGL = NT.Graphics.GL;
  var buffer = l3d.prepareModel(new NTGL.StructModel(vertices, colors, indeces, texture_coord), 
      new NTGL.StructAttrSize(3,4,2),
      new NTGL.StructLocation(attLocation[0], attLocation[1], attLocation[2]));

//  buffer.setTexture(l3d.createTexture("images/miku.png"));

  buffer.createTexture(l3d.gl, "/dungeons_holic/public_html/images/miku.png");

  return buffer;
}
