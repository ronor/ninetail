var width = 480;
var height = 270;

NT.scale = 1.0;
var nt = new NT.Core(width, height, 0x00 << 24 | 0x00 << 16 | 0x00 << 8 | 0xFF);

var L_3D = 100;
var scene = new NT.Scene("first");
nt.addScene(scene);

var layer = new NT.Graphics.Layer3D(width, height);
scene.addLayer(layer, L_3D);

var process = function(time) {
  nt.execute();
  window.requestAnimationFrame(process);
};


scene.registerBeginProcess(function() {
  console.log("scene1 begin");
  scene.getLayer(L_3D).setPriority(5);

  var l3d = scene.getLayer(L_3D);
  l3d.setVisible(true);

  if(!l3d.initShaders("vertex_shader", "fragment_shader")){
    console.log("ERROR: initShaders");
  }
});

scene.registerMainProcess(function() {

  var l3d = scene.getLayer(L_3D);
  var gl = l3d.gl;
  var attLocation = gl.getAttribLocation(l3d.program, 'aVertexPosition');
  var attStride = 3;
  var vertices = [
    0.0, 1.0, 0.0,
    1.0, 0.0, 0.0,
    -1.0,0.0,0.0
  ];


  //Create VBO
  var vbo = l3d.createVBO(vertices);
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.enableVertexAttribArray(attLocation);
  gl.vertexAttribPointer(attLocation, attStride, gl.FLOAT, false, 0, 0);

  var mat = NT.MatrixUtil;

  /*
  var vMatrix = mat.identity(mat.create());
  var mMatrix = mat.identity(mat.create());
  var pMatrix = mat.identity(mat.create());
  var mvMatrix = mat.identity(mat.create());
  var mvpMatrix = mat.identity(mat.create());
  */

  var m = new matIV();
  var mMatrix = m.identity(m.create());
  var vMatrix = m.identity(m.create());
  var pMatrix = m.identity(m.create());
  var mvpMatrix = m.identity(m.create());
  

  m.lookAt([0.0, 1.0, 3.0], [0, 0, 0], [0, 1, 0], vMatrix);
  m.perspective(90, width / height, 0.1, 100, pMatrix);

  m.multiply(pMatrix, vMatrix, mvpMatrix);
  m.multiply(mvpMatrix, mMatrix, mvpMatrix);

  var uniLocation = gl.getUniformLocation(l3d.program, 'mvpMatrix');
  gl.uniformMatrix4fv(uniLocation, false, mvpMatrix);

  /*
  mat.multiply(vMatrix, mMatrix, mvMatrix);
  var uniPLocation = gl.getUniformLocation(l3d.program, 'uPMatrix');
  var uniMVLocation = gl.getUniformLocation(l3d.program, 'uMVMatrix');
  gl.uniformMatrix4fv(uniPLocation, false, pMatrix);
  gl.uniformMatrix4fv(uniMVLocation, false, mvMatrix);
  */

  gl.clearColor(1.0, 0.0, 0.0, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  gl.enable(gl.DEPTH_TEST);

  gl.drawArrays(gl.TRIANGLES, 0, 3);
  gl.flush();
  /*
  for(var i=0; i<balls.length; i++) {
    ball_x[i] += move_x[i];
    ball_y[i] += move_y[i];

    if (ball_x[i] > 480 - balls[i].width) {
      move_x[i] = -1 * (Math.floor(Math.random() * 3) + 1);
    } else if (ball_x[i] < 0) {
      move_x[i] =  Math.floor(Math.random() * 3) + 1;
    }

    if (ball_y[i] > 270 - balls[i].height) { 
      move_y[i] = -1 * (Math.floor(Math.random() * 3) + 1);
    } else if (ball_y[i] < 0) {
      move_y[i] = Math.floor(Math.random() * 3) + 1;
    }

    balls[i].setPositionX(ball_x[i]);
    balls[i].setPositionY(ball_y[i]);

  }
  */

  /*
  var color = 0x00 << 24 | 0xFF << 16 | 0xFF << 8 | 0xFF;
  layers[1].setForegroundColor(color);
  layers[1].drawLine(1,1,200,200);
  */

});

scene.registerEndProcess(function() {
  console.log("scene1 end");
});

nt.changeScene("first");

window.requestAnimationFrame(process);
