var NT;
(function (NT) {
    var Box = (function () {
        function Box(width, height) {
            this.width = width;
            this.height = height;
        }
        return Box;
    })();
    NT.Box = Box;
})(NT || (NT = {}));
var NT;
(function (NT) {
    var Core = (function () {
        function Core(width, height, background_color) {
            this.width = width;
            this.height = height;
            this.state = 0 /* Play */;
            this.scenes = {};
            this.current_scene_name = null;
            this.state = 0 /* Play */;

            var user_agent = window.navigator.userAgent.toLowerCase();
            if (user_agent.indexOf("mobile") != -1) {
                NT.device_type = "mobile";
            } else {
                NT.device_type = "other";
            }

            var screen_width = document.documentElement.clientWidth;
            var screen_height = document.documentElement.clientHeight;
            var body = document.getElementsByTagName("body");
            body[0].style.width = screen_width + "px";
            body[0].style.height = screen_height + "px";
            if (background_color) {
                body[0].style.backgroundColor = NT.Graphics.Color.toCss(background_color);
            }
            body[0].style.display = "table-cell";
            body[0].style.verticalAlign = "middle";
            body[0].style.margin = "0";

            var container = document.createElement("div");
            container.id = NT.viewport;
            container.style.overflow = "hidden";
            container.style.width = (width * NT.scale) + "px";
            container.style.height = (height * NT.scale) + "px";
            container.style.position = "relative";
            container.style.margin = "0 auto";
            document.body.appendChild(container);
        }
        Core.prototype.addScene = function (scene) {
            this.scenes[scene.getName()] = scene;
        };

        Core.prototype.changeScene = function (scene_name) {
            if (this.current_scene_name != null) {
                this.scenes[this.current_scene_name].state = 2 /* End */;
                this.scenes[this.current_scene_name].execute();
            }

            this.current_scene_name = scene_name;
            this.scenes[this.current_scene_name].state = 0 /* Begin */;
        };

        Core.prototype.execute = function () {
            if (this.state == 0 /* Play */) {
                if (this.current_scene_name != null) {
                    this.scenes[this.current_scene_name].execute();
                } else {
                    console.log("please set scene");
                }
            }
        };

        Core.prototype.play = function () {
            this.state = 0 /* Play */;
        };

        Core.prototype.pause = function () {
            this.state = 1 /* Pause */;
        };
        return Core;
    })();
    NT.Core = Core;
})(NT || (NT = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var NT;
(function (NT) {
    var Vector2 = (function () {
        function Vector2(a, b) {
            if (a == null) {
                this.x = 0;
                this.y = 0;
            } else {
                if (a instanceof Vector2) {
                    this.x = a.x;
                    this.y = a.y;
                } else {
                    this.x = a;
                    this.y = b;
                }
            }
        }
        return Vector2;
    })();
    NT.Vector2 = Vector2;

    var Vector3 = (function (_super) {
        __extends(Vector3, _super);
        function Vector3(x, y, z) {
            _super.call(this, x, y);
            this.z = z;
        }
        return Vector3;
    })(Vector2);
    NT.Vector3 = Vector3;

    var Matrix = (function () {
        function Matrix(data) {
            if (!data) {
                this.data = new Float32Array(16);
            } else {
                this.data = data;
            }
        }
        Matrix.prototype.copy = function () {
            return new NT.Matrix(this.data);
        };

        Matrix.prototype.identity = function () {
            for (var i = 0; i < this.data.length; i++) {
                this.data[i] = (i % 5 == 0) ? 1 : 0;
            }
            return this;
        };

        Matrix.prototype.x = function (mat2) {
            return this.multiply(mat2);
        };

        Matrix.prototype.translate = function (vec) {
            var dest = new Float32Array(16);
            var mat = this.data;
            dest[0] = mat[0];
            dest[1] = mat[1];
            dest[2] = mat[2];
            dest[3] = mat[3];
            dest[4] = mat[4];
            dest[5] = mat[5];
            dest[6] = mat[6];
            dest[7] = mat[7];
            dest[8] = mat[8];
            dest[9] = mat[9];
            dest[10] = mat[10];
            dest[11] = mat[11];
            dest[12] = mat[0] * vec.x + mat[4] * vec.y + mat[8] * vec.z + mat[12];
            dest[13] = mat[1] * vec.x + mat[5] * vec.y + mat[9] * vec.z + mat[13];
            dest[14] = mat[2] * vec.x + mat[6] * vec.y + mat[10] * vec.z + mat[14];
            dest[15] = mat[3] * vec.x + mat[7] * vec.y + mat[11] * vec.z + mat[15];

            this.data = dest;
            return this;
        };

        Matrix.prototype.scale = function (vec) {
            var dest = new Float32Array(16);
            var mat = this.data;
            dest[0] = mat[0] * vec.x;
            dest[1] = mat[1] * vec.x;
            dest[2] = mat[2] * vec.x;
            dest[3] = mat[3] * vec.x;
            dest[4] = mat[4] * vec.y;
            dest[5] = mat[5] * vec.y;
            dest[6] = mat[6] * vec.y;
            dest[7] = mat[7] * vec.y;
            dest[8] = mat[8] * vec.z;
            dest[9] = mat[9] * vec.z;
            dest[10] = mat[10] * vec.z;
            dest[11] = mat[11] * vec.z;
            dest[12] = mat[12];
            dest[13] = mat[13];
            dest[14] = mat[14];
            dest[15] = mat[15];
            this.data = dest;
            return this;
        };

        Matrix.prototype.rotate = function (rad, axis) {
            var dest = new Float32Array(16);
            var mat = this.data;
            var sq = Math.sqrt(axis.x * axis.x + axis.y * axis.y + axis.z * axis.z);
            if (!sq)
                return null;

            var a = axis.x;
            var b = axis.y;
            var c = axis.z;

            if (sq != 1) {
                sq = 1 / sq;
                a *= sq;
                b *= sq;
                c *= sq;
            }

            var d = Math.sin(rad), e = Math.cos(rad), f = 1 - e, g = mat[0], h = mat[1], i = mat[2], j = mat[3], k = mat[4], l = mat[5], m = mat[6], n = mat[7], o = mat[8], p = mat[9], q = mat[10], r = mat[11], s = a * a * f + e, t = b * a * f + c * d, u = c * a * f - b * d, v = a * b * f - c * d, w = b * b * f + e, x = c * b * f + a * d, y = a * c * f + b * d, z = b * c * f - a * d, A = c * c * f + e;
            if (rad) {
                if (mat != dest) {
                    dest[12] = mat[12];
                    dest[13] = mat[13];
                    dest[14] = mat[14];
                    dest[15] = mat[15];
                }
            } else {
                dest = mat;
            }
            dest[0] = g * s + k * t + o * u;
            dest[1] = h * s + l * t + p * u;
            dest[2] = i * s + m * t + q * u;
            dest[3] = j * s + n * t + r * u;
            dest[4] = g * v + k * w + o * x;
            dest[5] = h * v + l * w + p * x;
            dest[6] = i * v + m * w + q * x;
            dest[7] = j * v + n * w + r * x;
            dest[8] = g * y + k * z + o * A;
            dest[9] = h * y + l * z + p * A;
            dest[10] = i * y + m * z + q * A;
            dest[11] = j * y + n * z + r * A;
            this.data = dest;
            return this;
        };

        Matrix.prototype.multiply = function (mat2) {
            var dest = new Float32Array(16);
            var m1 = this.data;
            var m2 = mat2.data;
            var a = m1[0], b = m1[1], c = m1[2], d = m1[3], e = m1[4], f = m1[5], g = m1[6], h = m1[7], i = m1[8], j = m1[9], k = m1[10], l = m1[11], m = m1[12], n = m1[13], o = m1[14], p = m1[15], A = m2[0], B = m2[1], C = m2[2], D = m2[3], E = m2[4], F = m2[5], G = m2[6], H = m2[7], I = m2[8], J = m2[9], K = m2[10], L = m2[11], M = m2[12], N = m2[13], O = m2[14], P = m2[15];
            dest[0] = A * a + B * e + C * i + D * m;
            dest[1] = A * b + B * f + C * j + D * n;
            dest[2] = A * c + B * g + C * k + D * o;
            dest[3] = A * d + B * h + C * l + D * p;
            dest[4] = E * a + F * e + G * i + H * m;
            dest[5] = E * b + F * f + G * j + H * n;
            dest[6] = E * c + F * g + G * k + H * o;
            dest[7] = E * d + F * h + G * l + H * p;
            dest[8] = I * a + J * e + K * i + L * m;
            dest[9] = I * b + J * f + K * j + L * n;
            dest[10] = I * c + J * g + K * k + L * o;
            dest[11] = I * d + J * h + K * l + L * p;
            dest[12] = M * a + N * e + O * i + P * m;
            dest[13] = M * b + N * f + O * j + P * n;
            dest[14] = M * c + N * g + O * k + P * o;
            dest[15] = M * d + N * h + O * l + P * p;

            return new NT.Matrix(dest);
        };
        return Matrix;
    })();
    NT.Matrix = Matrix;
})(NT || (NT = {}));
var NT;
(function (NT) {
    var MatrixUtil = (function () {
        function MatrixUtil() {
        }
        MatrixUtil.lookAt = function (eye, center, up, dest_matrix) {
            var eye_x = eye[0];
            var eye_y = eye[1];
            var eye_z = eye[2];
            var up_x = up[0];
            var up_y = up[1];
            var up_z = up[2];
            var center_x = center[0];
            var center_y = center[1];
            var center_z = center[2];

            if (eye_x == center_x && eye_y == center_y && eye_z == center_z) {
                return;
            }
            var x0, x1, x2;
            var y0, y1, y2;
            var z0, z1, z2;
            var l;

            z0 = eye_x - center[0];
            z1 = eye_y - center[1];
            z2 = eye_z - center[2];

            l = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);

            z0 *= l;
            z1 *= l;
            z2 *= l;

            x0 = up_y * z2 - up_z * z1;
            x1 = up_z * z0 - up_x * z2;
            x2 = up_x * z1 - up_y * z0;

            l = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);

            if (!l) {
                x0 = 0;
                x1 = 0;
                x2 = 0;
            } else {
                l = 1 / l;
                x0 *= l;
                x1 *= l;
                x2 *= l;
            }

            y0 = z1 * x2 - z2 * x1;
            y1 = z2 * x0 - z0 * x2;
            y2 = z0 * x1 - z1 * x0;

            l = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
            if (!l) {
                y0 = 0;
                y1 = 0;
                y2 = 0;
            } else {
                l = 1 / l;
                y0 *= l;
                y1 *= l;
                y2 *= l;
            }

            var dest = dest_matrix.data;

            dest[0] = x0;
            dest[1] = y0;
            dest[2] = z0;
            dest[3] = 0;
            dest[4] = x1;
            dest[5] = y1;
            dest[6] = z1;
            dest[7] = 0;
            dest[8] = x2;
            dest[9] = y2;
            dest[10] = z2;
            dest[11] = 0;
            dest[12] = -(x0 * eye_x + x1 * eye_y + x2 * eye_z);
            dest[13] = -(y0 * eye_x + y1 * eye_y + y2 * eye_z);
            dest[14] = -(z0 * eye_x + z1 * eye_y + z2 * eye_z);
            dest[15] = 1;

            dest_matrix.data = dest;
        };

        MatrixUtil.perspective = function (fovy, aspect, near, far, dest_matrix) {
            var t = near * Math.tan(fovy * Math.PI / 360);
            var r = t * aspect;
            var a = r * 2, b = t * 2, c = far - near;
            var dest = dest_matrix.data;
            dest[0] = near * 2 / a;
            dest[1] = 0;
            dest[2] = 0;
            dest[3] = 0;
            dest[4] = 0;
            dest[5] = near * 2 / b;
            dest[6] = 0;
            dest[7] = 0;
            dest[8] = 0;
            dest[9] = 0;
            dest[10] = -(far + near) / c;
            dest[11] = -1;
            dest[12] = 0;
            dest[13] = 0;
            dest[14] = -(far * near * 2) / c;
            dest[15] = 0;

            dest_matrix.data = dest;
        };
        return MatrixUtil;
    })();
    NT.MatrixUtil = MatrixUtil;
})(NT || (NT = {}));
var NT;
(function (NT) {
    NT.viewport = "viewport";
    NT.device_type = undefined;

    NT.scale = 1;

    (function (SceneState) {
        SceneState[SceneState["Begin"] = 0] = "Begin";
        SceneState[SceneState["Main"] = 1] = "Main";
        SceneState[SceneState["End"] = 2] = "End";
    })(NT.SceneState || (NT.SceneState = {}));
    var SceneState = NT.SceneState;
    ;

    (function (GameState) {
        GameState[GameState["Play"] = 0] = "Play";
        GameState[GameState["Pause"] = 1] = "Pause";
    })(NT.GameState || (NT.GameState = {}));
    var GameState = NT.GameState;
    ;
})(NT || (NT = {}));
var NT;
(function (NT) {
    var Point = (function () {
        function Point(x, y) {
            this.x = x;
            this.y = y;
        }
        Point.prototype.getX = function () {
            return this.x;
        };

        Point.prototype.getY = function () {
            return this.y;
        };
        return Point;
    })();
    NT.Point = Point;
})(NT || (NT = {}));
var NT;
(function (NT) {
    var Rect = (function () {
        function Rect(x, y, width, height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
        Rect.prototype.containsPoint = function (point) {
            if (point.getX() >= this.x && point.getX() <= this.x + this.width && point.getY() >= this.y && point.getY() <= this.y + this.height) {
                return true;
            } else {
                return false;
            }
        };
        return Rect;
    })();
    NT.Rect = Rect;
})(NT || (NT = {}));
var NT;
(function (NT) {
    var Scene = (function () {
        function Scene(scene_name) {
            this.scene_name = scene_name;
            this.mouse_event = {};
            this.state = 0 /* Begin */;
            this.layers = {};
        }
        Scene.prototype.init = function () {
            var event_type = ["mousedown", "mouseup", "mousemove"];
            var event_type_touch = ["touchstart", "touchend", "touchmove"];
            for (var i = 0; i < event_type.length; i++) {
                if (this.mouse_event[event_type[i]] != undefined) {
                    if (window.addEventListener) {
                        if (NT.device_type == "mobile") {
                            document.addEventListener(event_type_touch[i], this.mouse_event[event_type[i]], true);
                        } else {
                            document.addEventListener(event_type[i], this.mouse_event[event_type[i]], true);
                        }
                    }
                }
            }

            for (var key in this.layers) {
                console.log("init layer:" + key);
                this.layers[key].createCanvas();
                this.layers[key].initContext();
            }
        };

        Scene.prototype.registerBeginProcess = function (f) {
            if (this.begin_process == undefined)
                this.begin_process = f;
        };

        Scene.prototype.registerMainProcess = function (f) {
            if (this.main_process == undefined)
                this.main_process = f;
        };

        Scene.prototype.registerEndProcess = function (f) {
            if (this.end_process == undefined)
                this.end_process = f;
        };

        Scene.prototype.addEvent = function (type, f) {
            if (this.mouse_event[type] == undefined)
                this.mouse_event[type] = f;
        };

        Scene.prototype.getName = function () {
            return this.scene_name;
        };

        Scene.prototype.addLayer = function (layer, tag) {
            this.layers[tag] = layer;
            console.log(this.layers[tag]);
        };

        Scene.prototype.getLayer = function (tag) {
            return this.layers[tag];
        };

        Scene.prototype.execute = function () {
            if (this.state == 0 /* Begin */) {
                this.init();
                if (this.begin_process != undefined)
                    this.begin_process.apply(null);
                this.state = 1 /* Main */;
            } else if (this.state == 1 /* Main */) {
                if (this.main_process != undefined)
                    this.main_process.apply(null);
                this.draw();
            } else if (this.state == 2 /* End */) {
                if (this.end_process != undefined)
                    this.end_process.apply(null);
                this.destroy();
            }
        };

        Scene.prototype.destroy = function () {
            for (var key in this.layers) {
                this.layers[key].destroy();
            }

            var event_type = ["mousedown", "mouseup", "mousemove"];
            var event_type_touch = ["touchstart", "touchend", "touchmove"];
            for (var i = 0; i < event_type.length; i++) {
                if (this.mouse_event[event_type[i]] != undefined) {
                    if (window.removeEventListener) {
                        if (NT.device_type == "mobile") {
                            document.removeEventListener(event_type_touch[i], this.mouse_event[event_type[i]], true);
                        } else {
                            document.removeEventListener(event_type[i], this.mouse_event[event_type[i]], true);
                        }
                    }
                }
            }
        };

        Scene.prototype.draw = function () {
            for (var key in this.layers) {
                if (this.layers[key].getVisible())
                    this.layers[key].draw(null);
            }
        };
        return Scene;
    })();
    NT.Scene = Scene;
})(NT || (NT = {}));
var NT;
(function (NT) {
    var Stack = (function () {
        function Stack() {
            this.STACK_MAX = 60;
            this.data = new Array(this.STACK_MAX);

            this.top = 0;
        }
        Stack.prototype.push = function (v) {
            if (this.top != this.STACK_MAX) {
                this.data[this.top] = v;
                this.top++;
            }
        };

        Stack.prototype.pop = function () {
            if (this.top != 0) {
                this.top--;
                return this.data[this.top];
            } else {
                return null;
            }
        };
        return Stack;
    })();
    NT.Stack = Stack;
})(NT || (NT = {}));
var NT;
(function (NT) {
    (function (Graphics) {
        var Bitmap = (function () {
            function Bitmap(file) {
                var _this = this;
                this.is_loaded = false;
                this.image = new Image();
                this.image.src = file + "?" + new Date().getTime();
                this.image.onload = function () {
                    _this.is_loaded = true;
                };
            }
            Bitmap.prototype.getImage = function () {
                return this.image;
            };

            Bitmap.prototype.isLoaded = function () {
                return this.is_loaded;
            };
            return Bitmap;
        })();
        Graphics.Bitmap = Bitmap;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));
var NT;
(function (NT) {
    (function (Graphics) {
        var Color = (function () {
            function Color(r, g, b, a) {
                this.r = r;
                this.g = g;
                this.b = b;
                this.a = a;
                if (!a) {
                    this.a = 0xFF;
                }
            }
            Color.toCss = function (color) {
                var r = color >> 24 & 0xFF;
                var g = color >> 16 & 0xFF;
                var b = color >> 8 & 0xFF;
                var a = color & 0xFF;
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            Color.prototype.getR = function () {
                return this.r;
            };

            Color.prototype.getG = function () {
                return this.g;
            };

            Color.prototype.getB = function () {
                return this.b;
            };

            Color.prototype.getAlpha = function () {
                return this.a;
            };

            Color.prototype.getFloatR = function () {
                return this.r / 255;
            };

            Color.prototype.getFloatG = function () {
                return this.g / 255;
            };

            Color.prototype.getFloatB = function () {
                return this.b / 255;
            };

            Color.prototype.getFloatAlpha = function () {
                return this.a / 255;
            };

            Color.prototype.getCss = function () {
                return "rgb(" + this.r + "," + this.g + "," + this.b + ")";
            };
            return Color;
        })();
        Graphics.Color = Color;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));
var NT;
(function (NT) {
    (function (Graphics) {
        var DrawableElement = (function () {
            function DrawableElement(width, height) {
                this.x = 0;
                this.y = 0;
                this.is_visible = true;

                if (width == undefined && height == undefined) {
                    this.width = 0;
                    this.height = 0;
                } else {
                    this.width = width;
                    this.height = height;
                }
                this.drawable_elements = {};
            }
            DrawableElement.prototype.addElement = function (node, tag) {
                this.drawable_elements[tag] = node;
            };

            DrawableElement.prototype.getElement = function (tag) {
                return this.drawable_elements[tag];
            };

            DrawableElement.prototype.draw = function (ctx) {
                if (this.background_color != undefined) {
                    ctx.fillStyle = NT.Graphics.Color.toCss(this.background_color);
                    ctx.fillRect(this.x, this.y, this.width, this.height);
                }
            };

            DrawableElement.prototype.destroy = function () {
            };

            DrawableElement.prototype.setVisible = function (b) {
                this.is_visible = b;
            };

            DrawableElement.prototype.getVisible = function () {
                return this.is_visible;
            };

            DrawableElement.prototype.setPosition = function (x, y) {
                this.x = x;
                this.y = y;
            };

            DrawableElement.prototype.setPositionX = function (x) {
                this.x = x;
            };

            DrawableElement.prototype.setPositionY = function (y) {
                this.y = y;
            };

            DrawableElement.prototype.getPosition = function () {
                return new NT.Point(this.x, this.y);
            };

            DrawableElement.prototype.getPositionX = function () {
                return this.x;
            };

            DrawableElement.prototype.getPositionY = function () {
                return this.y;
            };

            DrawableElement.prototype.setPriority = function (priority_index) {
                console.log(priority_index);
                this.priority_index = priority_index;
            };

            DrawableElement.prototype.getPriority = function () {
                return this.priority_index;
            };

            DrawableElement.prototype.setForegroundColor = function (foreground_color) {
                this.foreground_color = foreground_color;
            };

            DrawableElement.prototype.getForegroundColor = function () {
                return this.foreground_color;
            };

            DrawableElement.prototype.setBackgroundColor = function (background_color) {
                this.background_color = background_color;
            };

            DrawableElement.prototype.getBackgroundColor = function () {
                return this.background_color;
            };
            return DrawableElement;
        })();
        Graphics.DrawableElement = DrawableElement;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));
var NT;
(function (NT) {
    (function (Graphics) {
        var Font = (function () {
            function Font(family, size, format, style) {
                this.family = "Arial";
                this.size = 20;
                this.format = "pt";
                this.style = "normal";
                if (family)
                    this.family = family;
                if (size)
                    this.size = size;
                if (format)
                    this.format = format;
                if (style)
                    this.style = style;
            }
            Font.prototype.getCss = function () {
                return this.style + " " + this.size + this.format + " " + this.family;
            };
            return Font;
        })();
        Graphics.Font = Font;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));
var NT;
(function (NT) {
    (function (Graphics) {
        var Layer = (function (_super) {
            __extends(Layer, _super);
            function Layer(width, height, background_color) {
                _super.call(this, width, height);
                this.setBackgroundColor(background_color);
            }
            Layer.prototype.createCanvas = function () {
                var container = document.getElementById(NT.viewport);
                if (this.width == 0 && this.height == 0) {
                    this.width = parseInt(container.style.width);
                    this.height = parseInt(container.style.height);
                }

                this.canvas = document.createElement("canvas");
                this.canvas.style.width = (this.width * NT.scale) + "px";
                this.canvas.style.height = (this.height * NT.scale) + "px";
                this.canvas.style.position = "absolute";
                this.canvas.style.top = "0";
                this.canvas.style.left = "0";

                if (this.background_color) {
                    console.log("BG COLOR = " + this.background_color);
                    this.canvas.style.backgroundColor = NT.Graphics.Color.toCss(this.background_color);
                }
                this.canvas.setAttribute('width', this.width.toFixed());
                this.canvas.setAttribute('height', this.height.toFixed());
                container.appendChild(this.canvas);
            };

            Layer.prototype.initContext = function () {
                this.ctx = this.canvas.getContext("2d");
            };

            Layer.prototype.clearCanvas = function () {
                this.ctx.clearRect(0, 0, this.width, this.height);
            };

            Layer.prototype.draw = function (ctx) {
                if (this.ctx) {
                    this.clearCanvas();
                    for (var key in this.drawable_elements) {
                        if (this.drawable_elements[key].getVisible()) {
                            this.drawable_elements[key].draw(this.ctx);
                        }
                    }
                }
            };

            Layer.prototype.destroy = function () {
                var container = document.getElementById(NT.viewport);
                for (var i = 0; i < this.canvas.childNodes.length; i++) {
                    this.canvas.removeChild(this.canvas.childNodes[i]);
                }
                container.removeChild(this.canvas);
            };

            Layer.prototype.setPriority = function (priority_index) {
                _super.prototype.setPriority.call(this, priority_index);
                this.canvas.style.zIndex = priority_index + '';
            };
            return Layer;
        })(Graphics.DrawableElement);
        Graphics.Layer = Layer;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));
var NT;
(function (NT) {
    (function (Graphics) {
        (function (GL) {
            var StructModelBuffer = (function () {
                function StructModelBuffer(vbo, cvbo, ibo, tvbo) {
                    this.vbo = vbo;
                    this.cvbo = cvbo;
                    this.ibo = ibo;

                    if (tvbo)
                        this.tvbo = tvbo;
                }
                StructModelBuffer.prototype.createTexture = function (gl, filename) {
                    var _this = this;
                    var texture_image = new Image();
                    var tex;
                    texture_image.onload = function () {
                        tex = gl.createTexture();

                        gl.bindTexture(gl.TEXTURE_2D, tex);

                        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_image);

                        gl.generateMipmap(gl.TEXTURE_2D);

                        gl.bindTexture(gl.TEXTURE_2D, null);

                        _this.texture = tex;
                    };
                    texture_image.src = filename;
                };

                StructModelBuffer.prototype.setTexture = function (tex) {
                    this.texture = tex;
                };

                StructModelBuffer.prototype.setIndexSize = function (index_size) {
                    this.index_size = index_size;
                };

                StructModelBuffer.prototype.setLocation = function (attr_location) {
                    this.attr_location = attr_location;
                };
                StructModelBuffer.prototype.setAttrSize = function (attr_size) {
                    this.attr_size = attr_size;
                };
                return StructModelBuffer;
            })();
            GL.StructModelBuffer = StructModelBuffer;

            var StructAttrSize = (function () {
                function StructAttrSize(vertex, color, texture) {
                    this.vertex = vertex;
                    this.color = color;
                    if (texture)
                        this.texture = texture;
                }
                return StructAttrSize;
            })();
            GL.StructAttrSize = StructAttrSize;

            var StructLocation = (function () {
                function StructLocation(vertex, color, texture) {
                    this.vertex = vertex;
                    this.color = color;
                    if (texture)
                        this.texture = texture;
                }
                return StructLocation;
            })();
            GL.StructLocation = StructLocation;

            var StructModel = (function () {
                function StructModel(vertex, color, index, texture_coord) {
                    this.vertex = vertex;
                    this.color = color;
                    this.index = index;
                    if (texture_coord) {
                        this.texture_coord = texture_coord;
                    }
                }
                return StructModel;
            })();
            GL.StructModel = StructModel;
        })(Graphics.GL || (Graphics.GL = {}));
        var GL = Graphics.GL;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));

var NT;
(function (NT) {
    (function (Graphics) {
        var Layer3D = (function (_super) {
            __extends(Layer3D, _super);
            function Layer3D(width, height, background_color) {
                _super.call(this, width, height, background_color);
            }
            Layer3D.prototype.initContext = function () {
                this._gl = this.canvas.getContext("webgl") || this.canvas.getContext("experimental-webgl");
            };

            Object.defineProperty(Layer3D.prototype, "gl", {
                get: function () {
                    return this._gl;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(Layer3D.prototype, "program", {
                get: function () {
                    return this._program;
                },
                set: function (program) {
                    this._program = program;
                },
                enumerable: true,
                configurable: true
            });


            Layer3D.prototype.setPriority = function (priority_index) {
                _super.prototype.setPriority.call(this, priority_index);
                this.canvas.style.zIndex = priority_index + '';
            };

            Layer3D.prototype.initShaders = function (vertex_shader_id, fragment_shader_id) {
                this.program = this.loadProgram(this.getShaderFromId(vertex_shader_id), this.getShaderFromId(fragment_shader_id));
                console.log(this.program);
                if (!this.program)
                    return false;
                this.gl.useProgram(this.program);
                return true;
            };

            Layer3D.prototype.setVertexPositionAttribute = function (position_name) {
                this.program.vertexPositionAttribute = this.gl.getAttribLocation(this.program, position_name);
                this.gl.enableVertexAttribArray(this.program.vertexPositionAttribute);
            };

            Layer3D.prototype.setVertexColorAttribute = function (vcolor_name) {
                this.program.vertexColorAttribute = this.gl.getAttribLocation(this.program, vcolor_name);
                this.gl.enableVertexAttribArray(this.program.vertexColorAttribute);
            };

            Layer3D.prototype.setMatrixUniform = function (p_matrix_name, mv_matrix_name) {
                this.program.pMatrixUniform = this.gl.getUniformLocation(this.program, p_matrix_name);
                this.program.mvMatrixUniform = this.gl.getUniformLocation(this.program, mv_matrix_name);
            };

            Layer3D.prototype.getShaderFromId = function (id) {
                var shader = null;
                var shader_script = document.getElementById(id);
                if (!shader_script) {
                    console.log("error:not found shader id");
                    return null;
                }

                var str = "";
                var k = shader_script.firstChild;
                while (k) {
                    if (k.nodeType == 3) {
                        str += k.textContent;
                    }
                    k = k.nextSibling;
                }

                if (shader_script.getAttribute("type") == "x-shader/x-fragment") {
                    shader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
                } else if (shader_script.getAttribute("type") == "x-shader/x-vertex") {
                    shader = this.gl.createShader(this.gl.VERTEX_SHADER);
                } else {
                    console.log("error: type is not valid");
                    return null;
                }

                this.gl.shaderSource(shader, str);
                this.gl.compileShader(shader);

                if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
                    console.log("SHADER COMPILE STATUS");
                    console.log(this.gl.getShaderInfoLog(shader));
                    return null;
                } else {
                    console.log(id + "COMPILE SUCCESS");
                }
                return shader;
            };

            Layer3D.prototype.loadProgram = function (vertex_shader, fragment_shader) {
                if (!vertex_shader || !fragment_shader) {
                    console.log("shader not found");
                    return null;
                }

                var program = this.gl.createProgram();

                this.gl.attachShader(program, vertex_shader);

                this.gl.attachShader(program, fragment_shader);

                this.gl.linkProgram(program);

                var linked = this.gl.getProgramParameter(program, this.gl.LINK_STATUS);
                if (!linked) {
                    var last_error = this.gl.getProgramInfoLog(program);
                    console.log("Error in program linking:" + last_error);
                    this.gl.deleteProgram(program);
                    return null;
                } else {
                    console.log("program linking success");
                }
                return program;
            };

            Layer3D.prototype.createVBO = function (data) {
                var vbo = this.gl.createBuffer();

                this.gl.bindBuffer(this.gl.ARRAY_BUFFER, vbo);

                this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(data), this.gl.STATIC_DRAW);

                this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
                return vbo;
            };

            Layer3D.prototype.createIBO = function (data) {
                var ibo = this.gl.createBuffer();
                this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, ibo);
                this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Int16Array(data), this.gl.STATIC_DRAW);
                this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, null);
                return ibo;
            };

            Layer3D.prototype.prepareModel = function (data, attr_size, attr_location) {
                var gl = this.gl;
                var vbo = this.createVBO(data.vertex);
                gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
                gl.enableVertexAttribArray(attr_location.vertex);
                gl.vertexAttribPointer(attr_location.vertex, attr_size.vertex, gl.FLOAT, false, 0, 0);

                var cvbo = this.createVBO(data.color);
                gl.bindBuffer(gl.ARRAY_BUFFER, cvbo);
                gl.enableVertexAttribArray(attr_location.color);
                gl.vertexAttribPointer(attr_location.color, attr_size.color, gl.FLOAT, false, 0, 0);

                var ibo = this.createIBO(data.index);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);

                var tvbo = null;
                if (data.texture_coord) {
                    tvbo = this.createVBO(data.texture_coord);
                    gl.bindBuffer(gl.ARRAY_BUFFER, tvbo);
                    gl.enableVertexAttribArray(attr_location.texture);
                    gl.vertexAttribPointer(attr_location.texture, attr_size.texture, gl.FLOAT, false, 0, 0);
                }

                var buffer = new NT.Graphics.GL.StructModelBuffer(vbo, cvbo, ibo, tvbo);
                buffer.setIndexSize(data.index.length);
                buffer.setAttrSize(attr_size);
                buffer.setLocation(attr_location);
                return buffer;
            };

            Layer3D.prototype.drawElement = function (buffer, num) {
                var gl = this.gl;
                gl.bindBuffer(gl.ARRAY_BUFFER, buffer.vbo);
                gl.vertexAttribPointer(buffer.attr_location.vertex, buffer.attr_size.vertex, gl.FLOAT, false, 0, 0);
                gl.bindBuffer(gl.ARRAY_BUFFER, buffer.cvbo);
                gl.vertexAttribPointer(buffer.attr_location.color, buffer.attr_size.color, gl.FLOAT, false, 0, 0);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer.ibo);

                if (buffer.texture) {
                    gl.bindTexture(gl.TEXTURE_2D, buffer.texture);
                }

                gl.drawElements(gl.TRIANGLES, buffer.index_size, gl.UNSIGNED_SHORT, 0);

                if (buffer.texture) {
                    gl.bindTexture(gl.TEXTURE_2D, null);
                }
            };
            return Layer3D;
        })(NT.Graphics.Layer);
        Graphics.Layer3D = Layer3D;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));
var NT;
(function (NT) {
    (function (Graphics) {
        var LayerBitmap = (function (_super) {
            __extends(LayerBitmap, _super);
            function LayerBitmap(width, height, color) {
                _super.call(this, width, height, color);
            }
            LayerBitmap.prototype.drawLine = function (x1, y1, x2, y2) {
                this.update_flag = true;

                var dx = 0;
                var dy = 0;

                dx = (x2 > x1) ? x2 - x1 : x1 - x2;
                dy = (y2 > y1) ? y2 - y1 : y1 - y2;

                var sx = 0;
                var sy = 0;

                sx = (x2 > x1) ? 1 : -1;
                sy = (y2 > y1) ? 1 : -1;

                if (dx > dy) {
                    var e = -dx;
                    for (var i = 0; i <= dx; i++) {
                        this.drawPixel(x1, y1);
                        x1 += sx;
                        e += 2 * dy;
                        if (e >= 0) {
                            y1 += sy;
                            e -= 2 * dx;
                        }
                    }
                } else {
                    var e = -dy;
                    for (var i = 0; i <= dy; i++) {
                        this.drawPixel(x1, y1);
                        y1 += sy;
                        e += 2 * dx;
                        if (e >= 0) {
                            x1 += sx;
                            e -= 2 * dy;
                        }
                    }
                }
            };

            LayerBitmap.prototype.drawPixel = function (x, y) {
                if (this.image_data) {
                    var width = this.image_data.width;
                    var height = this.image_data.height;
                    var index = ((width * y) + x) * 4;
                    var data = this.image_data.data;
                    data[index] = this.foreground_color >> 24 & 0xFF;
                    data[index + 1] = this.foreground_color >> 16 & 0xFF;
                    data[index + 2] = this.foreground_color >> 8 & 0xFF;
                    data[index + 3] = this.foreground_color;
                }
            };

            LayerBitmap.prototype.drawRect = function (x, y, width, height) {
            };

            LayerBitmap.prototype.fillRect = function (x, y, width, height) {
            };

            LayerBitmap.prototype.drawCircle = function (x, y, r) {
            };

            LayerBitmap.prototype.drawPolyline = function (points_x, points_y, points_num) {
            };

            LayerBitmap.prototype.draw = function (ctx) {
                if (this.image_data && this.update_flag) {
                    this.ctx.putImageData(this.image_data, 0, 0);
                    this.update_flag = false;
                }
                this.image_data = this.ctx.getImageData(0, 0, this.width, this.height);
            };
            return LayerBitmap;
        })(Graphics.Layer);
        Graphics.LayerBitmap = LayerBitmap;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));
var NT;
(function (NT) {
    (function (Graphics) {
        var Sprite = (function (_super) {
            __extends(Sprite, _super);
            function Sprite(bitmap, source_rect, width, height, background_color) {
                _super.call(this, width, height);
                _super.prototype.setBackgroundColor.call(this, background_color);

                if (bitmap) {
                    this.bitmap = bitmap;
                    if (source_rect)
                        this.source_rect = source_rect;
                }
            }
            Sprite.prototype.getRect = function () {
                return new NT.Rect(this.x, this.y, this.width, this.height);
            };

            Sprite.prototype.setText = function (text, font, color) {
                this.text = text;
                this.font = font;
                this.text_color = color;
            };

            Sprite.prototype.setSize = function (width, height) {
                this.width = width;
                this.height = height;
            };

            Sprite.prototype.draw = function (ctx) {
                _super.prototype.draw.call(this, ctx);

                if (this.text != undefined) {
                    ctx.fillStyle = this.text_color.getCss();
                    ctx.font = this.font.getCss();
                    ctx.fillText(this.text, this.x, this.y + 20);
                }

                if (this.bitmap != undefined) {
                    if (this.bitmap.isLoaded()) {
                        ctx.drawImage(this.bitmap.getImage(), (0.5 + this.source_rect.x) | 0, (0.5 + this.source_rect.y) | 0, (0.5 + this.source_rect.width) | 0, (0.5 + this.source_rect.height) | 0, (0.5 + this.x) | 0, (0.5 + this.y) | 0, (0.5 + this.width) | 0, (0.5 + this.height) | 0);
                    }
                }
            };
            return Sprite;
        })(Graphics.DrawableElement);
        Graphics.Sprite = Sprite;
    })(NT.Graphics || (NT.Graphics = {}));
    var Graphics = NT.Graphics;
})(NT || (NT = {}));
