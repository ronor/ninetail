'use strict'
module.exports = function(grunt) {
  grunt.initConfig({
    //TypeScriptビルド
    typescript: {
      base: {
        src: [
          './src/NT/*.ts', 
          './src/NT/Graphics/*.ts'
        ],
        dest: './js/libs/nt.js',
        options: {
          target: 'es5',
          sourcemap: false,
          declaration: false
        }
      }
    },
    watch: {
      doc: {
        files: [
          './src/NT/*.ts',
          './src/NT/Graphics/*.ts'
        ],
        tasks: [
          'typescript', 
          'uglify'
        ]
      }
    },
    //圧縮
    uglify: {
      dist: {
        files: {
          "./js/libs/nt.min.js": [
            "./js/libs/nt.js"
          ]
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-typescript');

  grunt.registerTask('default', ['typescript', 'uglify', 'watch']);
};
